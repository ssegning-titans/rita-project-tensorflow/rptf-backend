import {
  Controller,
  Get, Header,
  Param,
  ParseIntPipe,
  Query,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { FilesService } from './files.service';
import { ApiQuery, ApiTags } from '@nestjs/swagger';

@ApiTags('file')
@Controller('api/v1/files')
export class FilesController {
  constructor(private readonly filesService: FilesService) {}

  @Header('Cache-Control', 'max-age=31536000, immutable')
  @Get('photos/:file_name')
  public async download(
    @Param('file_name') fileName: string,
    @Res() res: Response,
  ) {
    const [stream, result] = await this.filesService.downloadFile(fileName);
    res.set({
      'Content-Type': result.contentType,
      'Content-Disposition': `inline; filename="${fileName}"`,
    });
    stream.pipe(res);
  }

  @ApiQuery({
    name: 'page',
    required: false,
    example: 0,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    example: 20,
    type: Number,
  })
  @Get()
  public async iterateFiles(
    @Query('page', ParseIntPipe) page = 0,
    @Query('limit', ParseIntPipe) limit = 20,
  ) {
    const skip = page * limit;
    return this.filesService.iterateFiles(skip, limit);
  }
}
