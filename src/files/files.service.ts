import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { IGridFSObject, MongoGridFS } from 'mongo-gridfs';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { GridFSBucketReadStream } from 'mongodb';
import { MONGODB_FILES_TOKEN } from '../types/token';

@Injectable()
export class FilesService {
  private readonly fileModel: MongoGridFS;

  constructor(
    @InjectConnection(MONGODB_FILES_TOKEN)
    private readonly connection: Connection,
  ) {
    // @ts-ignore
    this.fileModel = new MongoGridFS(connection.db, 'photos');
  }

  async findInfo(id: string) {
    const result = await this.fileModel.findById(id).catch((ignored) => {
      throw new HttpException('File not found', HttpStatus.NOT_FOUND);
    });

    return this.map(result);
  }

  public async downloadFile(
    filename: string,
  ): Promise<[GridFSBucketReadStream, IGridFSObject]> {
    try {
      const result = await this.fileModel.findOne({ filename });
      const stream = await this.fileModel.readFileStream(result._id.toString());
      stream.read();
      return [stream, result];
    } catch (error) {
      Logger.error(error);
      throw new HttpException(error, HttpStatus.NOT_FOUND);
    }
  }

  async readFile(id: string) {
    Logger.debug(`Looking for [${id}]`, 'Files');
    const readableStream = await this.fileModel
      .readFileStream(id)
      .catch((error) => {
        throw new HttpException(error, HttpStatus.NOT_FOUND);
      });

    return new Promise<Buffer>((resolve, reject) => {
      const bufferArray = [];

      readableStream.on('data', (chunk) => {
        bufferArray.push(chunk);
      });

      readableStream.on('end', () => {
        const buffer = Buffer.concat(bufferArray);
        resolve(buffer);
      });

      readableStream.on('error', (error) => {
        reject(error);
      });

      readableStream.read();
    });
  }

  async iterateFiles(skip = 0, limit = 20) {
    const results = await this.fileModel.bucket
      .find(
        {},
        {
          limit,
          skip,
          sort: 'uploadDate',
        },
      )
      .toArray();

    return results.map(this.map);
  }

  private map(result: IGridFSObject) {
    return {
      filename: result.filename,
      length: result.length,
      chunkSize: result.chunkSize,
      contentType: result.contentType,
      id: result._id,
      meta: result.metadata,
    };
  }
}
