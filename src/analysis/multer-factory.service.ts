import { Injectable } from '@nestjs/common';
import {
  MulterModuleOptions,
  MulterOptionsFactory,
} from '@nestjs/platform-express';
import { InjectConnection } from '@nestjs/mongoose';
import { MONGODB_FILES_TOKEN } from '../types/token';
import { Connection } from 'mongoose';
import { GridFsStorage } from 'multer-gridfs-storage';
import * as mime from 'mime-types';

@Injectable()
export class MulterFactoryService implements MulterOptionsFactory {
  constructor(
    @InjectConnection(MONGODB_FILES_TOKEN)
    private readonly connection: Connection,
  ) {}

  public async createMulterOptions(): Promise<MulterModuleOptions> {
    const db: any = this.connection.db;
    return {
      storage: new GridFsStorage({
        db,
        file: (req, file) => {
          const ext = mime.extension(file.mimetype);
          return {
            bucketName: 'photos',
            filename: 'file_' + Date.now() + '.' + ext,
          };
        },
      }),
    };
  }
}
