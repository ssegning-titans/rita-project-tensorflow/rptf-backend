import {
  Controller,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { AnalysisService } from './analysis.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { v4 as uuid } from 'uuid';
import { ApiBody, ApiConsumes, ApiQuery, ApiTags } from '@nestjs/swagger';

@ApiTags('analysis')
@Controller('api/v1/analysis')
export class AnalysisController {
  constructor(private readonly analysisService: AnalysisService) {}

  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiQuery({
    name: 'qr-code-id',
    required: false,
  })
  @ApiQuery({
    name: 'features',
    required: false,
    schema: {
      enum: ['COLOR', 'OBJ', 'TIME'],
    },
  })
  @ApiConsumes('multipart/form-data')
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Query('features') features: string[] | string,
    @Query('qr-code-id') qrCodeId?: string,
  ) {
    const trackId = uuid();
    const status = await this.analysisService.analysis(
      trackId,
      file,
      (Array.isArray(features) ? features : [features]).filter((i) => !!i),
      qrCodeId,
    );
    return { status, features, qrCodeId, trackId, fileName: file.filename };
  }
}
