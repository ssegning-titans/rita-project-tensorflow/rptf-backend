import { Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { AnalysisJobData } from '../types/analysis-job';
import { FeatureEnum } from '../types/features';

@Injectable()
export class AnalysisService {
  constructor(
    @InjectQueue('analysis')
    private readonly analysisQueue: Queue,
  ) {
  }

  public async analysis(
    trackId: string,
    file: Express.Multer.File,
    features: string[],
    qrCodeId?: string,
  ): Promise<string> {
    let status = 'ok';
    for (const feature of features) {
      const jobData = new AnalysisJobData(
        file,
        trackId,
        feature as FeatureEnum,
        qrCodeId,
      );
      const job = await this.analysisQueue.add(jobData);

      if (job.failedReason) {
        status = 'error';
      }
    }

    return status;
  }
}
