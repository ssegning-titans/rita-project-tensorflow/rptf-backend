import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import * as tf from '@tensorflow/tfjs';
import { InferenceModel } from '@tensorflow/tfjs';
import { ConfigService } from '@nestjs/config';

export const timeMapping = {
  0: 'afternoon',
  1: 'morning',
  2: 'night',
  3: 'noon',
};

export const colorMapping = {
  0: 'black',
  1: 'blue',
  2: 'green',
  3: 'purple',
  4: 'red',
  5: 'white',
  6: 'yellow',
};

@Injectable()
export class ModelService implements OnModuleInit {
  public readonly _models: Record<string, InferenceModel> = {};

  constructor(private readonly configService: ConfigService) {}

  public getModel<T extends InferenceModel>(feature: string): T {
    return this._models[feature] as T;
  }

  public async onModuleInit(): Promise<void> {
    const models = (this.configService.get<string>('MODELS') ?? '')
      .split(',')
      .filter((i) => !!i);

    for (const feature of models) {
      await this.loadModule(feature);
    }
  }

  private async loadModule(feature: string) {
    const model = await tf.loadLayersModel(
      `https://static.vymalo.com/random/rptf/output/${feature}/tfjs_model/model.json`,
    );

    this._models[feature.toUpperCase()] = model;
    Logger.log(`Feature loaded [${feature}]`, 'Models');
    model.summary();
  }
}
