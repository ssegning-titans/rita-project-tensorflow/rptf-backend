import * as tf from '@tensorflow/tfjs';
import { LayersModel } from '@tensorflow/tfjs';
import { ResultsService } from '../results/results.service';
import { OnQueueError, OnQueueFailed, Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { AnalysisJobData } from '../types/analysis-job';
import { QrCodeService } from '../qr-code/qr-code.service';
import { Logger } from '@nestjs/common';
import { CreateResultDto } from '../results/dto/create-result.dto';
import { FeatureEnum } from '../types/features';
import { FilesService } from '../files/files.service';
import { Predictions, UProb } from '../types/predictions';
import { colorMapping, ModelService, timeMapping } from './model.service';
import { createCanvas, loadImage } from 'canvas';
import * as _ from 'lodash';

const dim = 150;

@Processor('analysis')
export class AnalysisConsumer {
  constructor(
    private readonly resultsService: ResultsService,
    private readonly qrCodeService: QrCodeService,
    private readonly filesService: FilesService,
    private readonly modelService: ModelService,
  ) {}

  @Process()
  public async transcode(job: Job<AnalysisJobData>) {
    const feature = job.data.feature;

    Logger.log(
      `Started job [${job.id}] with feature [${job.data.feature}]`,
      feature,
    );
    // Start date of the processing:
    const startDate = new Date().getTime();

    // Get the Job Data
    const data = job.data;

    // Find the QrCode related to this
    const relatedQrCode = await this.qrCodeService
      .findOne(data.qrCodeId)
      .catch(() => null);

    Logger.log(`found related QrCode ? [${relatedQrCode?.id}]`, feature);
    // Get predictions
    const predictions = await this.doAnalyze(data.feature, data.file);

    Logger.log(`Did predictions`, feature);
    // Persist the result
    const result = new CreateResultDto();
    result.trackId = data.trackId;
    result.fileName = data.file.filename;
    result.feature = { name: data.feature };
    result.predictions = predictions;
    result.fileSize = data.file.size;
    result.startDate = startDate;
    result.endDate = new Date().getTime();
    result.relatedQrCode = relatedQrCode;
    const tmp = await this.resultsService.create(result);
    Logger.log(`Saved result [${tmp.id}]`, feature);
  }

  @OnQueueFailed()
  public handler(job: Job, error: Error) {
    Logger.error(error);
  }

  @OnQueueError()
  public onError(error: Error) {
    Logger.error(error);
  }

  private async doAnalyze(
    feature: FeatureEnum,
    file: Express.Multer.File,
  ): Promise<Predictions> {
    Logger.log(`Transform image first`, feature);
    // First get the image as buffers
    const imageBuffer = await this.filesService.readFile(file.id);

    Logger.log(`Got image buffer`, feature);
    // Create a canvas for preprocessing the image
    const canvas = createCanvas(dim, dim);
    const ctx = canvas.getContext('2d');

    Logger.log(`Changing image into canvas`, feature);
    // Load image into canvas
    const img = await loadImage(imageBuffer);
    ctx.drawImage(img, 0, 0, dim, dim);

    Logger.log(`Changing canvas into tensor image`, feature);
    // Convert canvas to tensor
    const imageTensor = tf.browser.fromPixels(canvas as any, 3);

    Logger.log(`Normalize the image`, feature);
    // Normalize image tensor
    const normalized = tf.image
      .resizeBilinear(imageTensor, [dim, dim])
      // .div(tf.scalar(255))
      .expandDims(0);

    Logger.log(`Now predict the normalized image`, feature);
    // Run image through model
    const predictions = await this.predict(feature, normalized);

    Logger.log(`Done. Looking for best of [${predictions.length}]`, feature);

    switch (feature) {
      case FeatureEnum.TIME:
      case FeatureEnum.COLOR:
        return this.mapToLabel(feature, predictions);
      default:
        throw new Error('this feature is not supported yet');
    }
  }

  private mapToLabel(feature: FeatureEnum, predictions: UProb[]): Predictions {
    const mapping =
      feature === FeatureEnum.TIME
        ? timeMapping
        : feature === FeatureEnum.COLOR
        ? colorMapping
        : undefined;

    const allProbabilityArrays = predictions.map((p) => Array.from(p));
    const probabilityArray = _.flatten(allProbabilityArrays);
    return _.reduce(
      probabilityArray,
      (prev, curr, idx) => {
        if (curr < 10e-5) return prev;
        return { ...prev, [mapping[idx]]: curr };
      },
      {},
    );
  }

  private async predict(
    feature: string,
    normalized: tf.Tensor<tf.Rank>,
  ): Promise<UProb[]> {
    Logger.log(`First look for the model`, feature);

    const model = this.modelService.getModel<LayersModel>(feature);
    Logger.log(`Model found`, feature);

    const predictedModel = await model.predict(normalized);
    if (Array.isArray(predictedModel)) {
      Logger.log(`>> Multiple predictions ${predictedModel.length}`, feature);
      return await Promise.all(predictedModel.map((i) => i.data()));
    } else {
      const tmp = await predictedModel.data();
      Logger.log(`>> One prediction: made`, feature);
      return [tmp];
    }
  }
}
