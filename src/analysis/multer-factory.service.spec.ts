import { Test, TestingModule } from '@nestjs/testing';
import { MulterFactoryService } from './multer-factory.service';

describe('MulterFactoryService', () => {
  let service: MulterFactoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MulterFactoryService],
    }).compile();

    service = module.get<MulterFactoryService>(MulterFactoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
