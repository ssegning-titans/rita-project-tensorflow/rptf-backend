import { Module } from '@nestjs/common';
import { AnalysisService } from './analysis.service';
import { AnalysisController } from './analysis.controller';
import { ResultsModule } from '../results/results.module';
import { BullModule } from '@nestjs/bull';
import { QrCodeModule } from '../qr-code/qr-code.module';
import { AnalysisConsumer } from './analysis.consumer';
import { ConfigModule } from '@nestjs/config';
import { FilesModule } from '../files/files.module';
import { MulterModule } from '@nestjs/platform-express';
import { ModelService } from './model.service';
import { MulterFactoryService } from './multer-factory.service';

@Module({
  controllers: [AnalysisController],
  providers: [
    AnalysisService,
    AnalysisConsumer,
    ModelService,
    MulterFactoryService,
  ],
  imports: [
    ConfigModule,
    FilesModule,
    // Multer configuration for uploading files
    MulterModule.registerAsync({
      useClass: MulterFactoryService,
    }),
    BullModule.registerQueue({
      name: 'analysis',
    }),
    QrCodeModule,
    ResultsModule,
  ],
})
export class AnalysisModule {}
