import { HttpException, Module } from '@nestjs/common';
import { ResultsModule } from './results/results.module';
import { QrCodeModule } from './qr-code/qr-code.module';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { AnalysisModule } from './analysis/analysis.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { BullModule } from '@nestjs/bull';
import { FilesModule } from './files/files.module';
import { DataMongoFactoryService } from './shared/services/data.mongo-factory/data.mongo-factory.service';
import { FileMongoFactoryService } from './shared/services/file.mongo-factory/file.mongo-factory.service';
import { SentryInterceptor, SentryModule } from '@ntegral/nestjs-sentry';
import { MONGODB_DATA_TOKEN, MONGODB_FILES_TOKEN } from './types/token';

@Module({
  imports: [
    // A rate limiter: 60s, 10 requests maximum
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 1000,
    }),

    // To load env variables
    ConfigModule.forRoot(),

    // Mongo configuration
    MongooseModule.forRootAsync({
      useClass: DataMongoFactoryService,
      imports: [ConfigModule],
      connectionName: MONGODB_DATA_TOKEN,
    }),

    // Mongo configuration
    MongooseModule.forRootAsync({
      useClass: FileMongoFactoryService,
      imports: [ConfigModule],
      connectionName: MONGODB_FILES_TOKEN,
    }),

    // Redis Queues configuration for asynchron operations
    BullModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const redisUri = configService.get<string>('REDIS_URI');
        return {
          url: redisUri,
        };
      },
      imports: [ConfigModule],
    }),

    // Sentry
    SentryModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        dsn: configService.get<string>('SENTRY_DSN'),
        debug: configService.get<string>('ENV') === 'production',
        environment: configService.get<string>('ENV'),
      }),
      imports: [ConfigModule],
    }),

    // Custom app Module
    ResultsModule, // 3th phase
    QrCodeModule, // 1th phase
    AnalysisModule, // 2th phase
    FilesModule, // All phases
  ],
  providers: [
    // To enable rate limiter for the whole application
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useFactory: () =>
        new SentryInterceptor({
          filters: [
            {
              type: HttpException,
              filter: (exception: HttpException) => 400 > exception.getStatus(), // Only report 500 errors
            },
          ],
        }),
    },
  ],
})
export class AppModule {}
