export type UProb = Uint8Array | Int32Array | Float32Array;

export type Predictions = Record<string, number>;
