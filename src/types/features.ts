import { IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export enum FeatureEnum {
  TIME = 'TIME',
  OBJ = 'OBJ',
  COLOR = 'COLOR',
}

export class Feature {
  @ApiProperty({
    enum: ['COLOR', 'OBJ', 'TIME'],
  })
  @IsEnum(FeatureEnum)
  name: FeatureEnum;
}
