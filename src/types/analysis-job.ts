import { FeatureEnum } from './features';

export class AnalysisJobData {
  constructor(
    public readonly file: Express.Multer.File,
    public readonly trackId: string,
    public readonly feature: FeatureEnum,
    public readonly qrCodeId: string,
  ) {
  }
}