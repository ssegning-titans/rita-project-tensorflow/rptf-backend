import { Test, TestingModule } from '@nestjs/testing';
import { DataMongoFactoryService } from './data.mongo-factory.service';

describe('DataMongoFactoryService', () => {
  let service: DataMongoFactoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DataMongoFactoryService],
    }).compile();

    service = module.get<DataMongoFactoryService>(DataMongoFactoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
