import { Test, TestingModule } from '@nestjs/testing';
import { FileMongoFactoryService } from './file.mongo-factory.service';

describe('FileMongoFactoryService', () => {
  let service: FileMongoFactoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FileMongoFactoryService],
    }).compile();

    service = module.get<FileMongoFactoryService>(FileMongoFactoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
