import { Injectable } from '@nestjs/common';
import { MongooseModuleOptions, MongooseOptionsFactory } from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class FileMongoFactoryService implements MongooseOptionsFactory {
  constructor(private readonly configService: ConfigService) {
  }

  public async createMongooseOptions(): Promise<MongooseModuleOptions> {
    const base = this.configService.get<string>('MONGODB_BASE_URI');
    return {
      uri: base,
      dbName: this.configService.get<string>('MONGODB_FILE_BUCKET'),
    };
  }
}
