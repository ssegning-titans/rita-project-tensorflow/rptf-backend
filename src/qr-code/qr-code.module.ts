import { Module } from '@nestjs/common';
import { QrCodeService } from './qr-code.service';
import { QrCodeController } from './controllers/qr-code.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { QrCode, QrCodeSchema } from './entities/qr-code.entity';
import { MONGODB_DATA_TOKEN } from '../types/token';

@Module({
  controllers: [QrCodeController],
  providers: [QrCodeService],
  exports: [QrCodeService],
  imports: [
    MongooseModule.forFeature(
      [
        {
          name: QrCode.name,
          schema: QrCodeSchema,
        },
      ],
      MONGODB_DATA_TOKEN,
    ),
  ],
})
export class QrCodeModule {
}
