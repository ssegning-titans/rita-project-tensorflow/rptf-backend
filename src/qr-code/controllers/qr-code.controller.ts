import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { QrCodeService } from '../qr-code.service';
import { CreateQrCodeDto } from '../dto/create-qr-code.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('qr-code')
@Controller('api/v1/qr-code')
export class QrCodeController {
  constructor(private readonly qrCodeService: QrCodeService) {
  }

  @Post()
  public async create(@Body() dto: CreateQrCodeDto) {
    return this.qrCodeService.create(dto);
  }

  @Get()
  public async findAll() {
    return this.qrCodeService.findAll();
  }

  @Get(':id')
  public async findOne(@Param('id') id: string) {
    return this.qrCodeService.findOne(id);
  }

  @Delete(':id')
  public async remove(@Param('id') id: string) {
    await this.qrCodeService.remove(id);
  }
}
