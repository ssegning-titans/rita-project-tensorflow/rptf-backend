import { HydratedDocument } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Feature } from '../../types/features';
import { ApiProperty } from '@nestjs/swagger';

@Schema()
export class QrCode {
  @ApiProperty()
  @Prop()
  name: string;

  @ApiProperty({
    type: [Feature],
  })
  @Prop([Feature])
  features: Feature[];
}

export type QrCodeDocument = HydratedDocument<QrCode>;

export const QrCodeSchema = SchemaFactory.createForClass(QrCode);