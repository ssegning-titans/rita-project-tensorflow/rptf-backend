import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateQrCodeDto } from './dto/create-qr-code.dto';
import { QrCode, QrCodeDocument } from './entities/qr-code.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MONGODB_DATA_TOKEN } from '../types/token';

@Injectable()
export class QrCodeService {
  constructor(
    @InjectModel(QrCode.name, MONGODB_DATA_TOKEN)
    private readonly qrCodeDocumentModel: Model<QrCodeDocument>,
  ) {}

  public async create(dto: CreateQrCodeDto) {
    const createdCat = new this.qrCodeDocumentModel(dto);
    return await createdCat.save();
  }

  public async findAll() {
    return await this.qrCodeDocumentModel.find().exec();
  }

  public async findOne(id: string) {
    const result = await this.qrCodeDocumentModel.findOne({ _id: id }).exec();
    if (!result) {
      throw new NotFoundException(`QR-Code with id [${id}] not found`);
    }

    return result;
  }

  public async remove(id: string) {
    const found = await this.findOne(id);
    await found.remove();
  }
}
