import { MessageBody, SubscribeMessage, WebSocketGateway, WsResponse } from '@nestjs/websockets';
import { ResultsService } from '../results.service';
import { from, map } from 'rxjs';

@WebSocketGateway()
export class ResultsGateway {

  constructor(
    private readonly resultsService: ResultsService,
  ) {
  }

  @SubscribeMessage('results')
  public handleMessage(@MessageBody() trackId: string) {
    const one = this.resultsService.findOne(trackId);

    return from(one).pipe(
      map((data) => ({ event: 'results', data } as WsResponse)),
    );
  }
}
