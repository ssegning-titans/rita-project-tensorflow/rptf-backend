import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateResultDto } from './dto/create-result.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Result, ResultDocument } from './entities/result.entity';
import { MONGODB_DATA_TOKEN } from '../types/token';

@Injectable()
export class ResultsService {
  constructor(
    @InjectModel(Result.name, MONGODB_DATA_TOKEN)
    private readonly resultDocumentModel: Model<ResultDocument>,
  ) {}

  public async create(dto: CreateResultDto) {
    const createdCat = new this.resultDocumentModel(dto);
    return await createdCat.save();
  }

  public async findAll(limit = 10_000) {
    return this.resultDocumentModel.find().limit(limit).exec();
  }

  public async searchBySameOptions(
    feature: string,
    minProbability: number,
    option: string,
    limit = 10_000,
  ) {
    return this.resultDocumentModel
      .find({
        'feature.name': feature,
        ['predictions.' + option]: {
          $gte: Math.min(minProbability, 0),
          $lte: 1,
        },
      })
      .limit(limit)
      .exec();
  }

  public async findOne(id: string) {
    const result = await this.resultDocumentModel.findById(id).exec();
    if (!result) {
      throw new NotFoundException(`Result with id [${id}] not found`);
    }

    return result;
  }
}
