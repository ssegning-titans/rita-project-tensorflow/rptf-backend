import { Module } from '@nestjs/common';
import { ResultsService } from './results.service';
import { ResultsController } from './controllers/results.controller';
import { ResultsGateway } from './gateway/results.gateway';
import { MongooseModule } from '@nestjs/mongoose';
import { Result, ResultSchema } from './entities/result.entity';
import { MONGODB_DATA_TOKEN } from '../types/token';

@Module({
  controllers: [ResultsController],
  providers: [ResultsService, ResultsGateway],
  exports: [ResultsService],
  imports: [
    MongooseModule.forFeature(
      [
        {
          name: Result.name,
          schema: ResultSchema,
        },
      ],
      MONGODB_DATA_TOKEN,
    ),
  ],
})
export class ResultsModule {
}
