import { IsNotEmpty, IsNumber } from 'class-validator';

export class FindResultDto {
  @IsNotEmpty()
  feature: string;

  @IsNumber()
  minProbability: number;

  @IsNotEmpty()
  option: string;

  @IsNumber()
  limit: number;
}
