import { QrCode } from '../../qr-code/entities/qr-code.entity';
import {
  IsNotEmpty,
  IsNumber,
  IsObject,
  ValidateNested,
} from 'class-validator';
import { Feature } from '../../types/features';
import { Predictions } from '../../types/predictions';

export class CreateResultDto {
  @IsNotEmpty()
  trackId: string;

  @IsNotEmpty()
  fileName: string;

  @IsObject()
  @ValidateNested()
  feature: Feature;

  predictions: Predictions;

  @IsNumber()
  fileSize: number;

  @IsNumber()
  startDate: number;

  @IsNumber()
  endDate: number;

  // Related QrCode, if any
  relatedQrCode?: QrCode;
}
