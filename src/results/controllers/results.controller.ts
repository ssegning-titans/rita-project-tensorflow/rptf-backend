import { Body, Controller, Get, HttpCode, Param, Post } from '@nestjs/common';
import { ResultsService } from '../results.service';
import { ApiTags } from '@nestjs/swagger';
import { FindResultDto } from '../dto/find-result.dto';

@ApiTags('results')
@Controller('api/v1/results')
export class ResultsController {
  constructor(private readonly resultsService: ResultsService) {}

  @Get()
  findAll() {
    return this.resultsService.findAll();
  }

  @HttpCode(200)
  @Post('search')
  searchBySameOptions(@Body() body: FindResultDto) {
    return this.resultsService.searchBySameOptions(
      body.feature,
      body.minProbability,
      body.option,
      body.limit,
    );
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.resultsService.findOne(id);
  }
}
