import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { HydratedDocument } from 'mongoose';
import { QrCode } from '../../qr-code/entities/qr-code.entity';
import { Feature } from '../../types/features';
import { Predictions } from '../../types/predictions';

export type ResultDocument = HydratedDocument<Result>;

@Schema()
export class Result {
  @Prop()
  trackId: string;

  @Prop()
  fileName: string;

  @Prop(Feature)
  feature: Feature;

  @Prop(raw({}))
  predictions: Predictions;

  @Prop()
  fileSize: number;

  @Prop()
  startDate: number;

  @Prop()
  endDate: number;

  // Related QrCode, if any
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'QrCode' })
  relatedQrCode?: QrCode;
}

export const ResultSchema = SchemaFactory.createForClass(Result);
