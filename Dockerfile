FROM node

ENV ENV 'production'

LABEL maintener="SSegning DEV <dev@ssegning.com>"

# RUN apt-get -qq update && apt-get -qq install -y python

WORKDIR /app

# copy from build image
COPY ./dist ./dist

COPY package.json yarn.lock ./

RUN yarn --frozen-lockfile --production --silent

EXPOSE 3000

ENTRYPOINT ["node", "dist/main"]
